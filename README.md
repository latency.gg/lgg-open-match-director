# Open-Match Director

A director service that integrates with Gameye as the Game Server Allocator.

## What does it do?

In the whole [picture](https://open-match.dev/site/images/architecture.png) of an Open-Match integration, the director is in charge of pulling established matches from Open-Match's backend service. Matches are made by putting inserted tickets (which is either one player or multiple as a group) into a pool that are then ran against a match function. The match function in the end decides which tickets belong together based on specific criterias (such as skill levels, latency etc). Once matches are pulled from the backend service, the director requests the game server allocator, which is in this case Gameye, to instantiate a session of a specific image. Once the session has started, an IP address-and port number is returned that the director assigns to the tickets involved in that specific match for the frontend service to deal with. The job of the director then ends there and continues waiting for new matches.

The director can be hosted in either the same cluster as that of Open-Match or externally with Open-Match's backend service being exposed to the outside world, where the former scenario is probably more favourable.

## Getting Started

**Prerequisite**: A local Open-Match cluster running on Minikube. To learn how to set one up, check [this](https://gitlab.com/latency.gg/lgg-open-match-test-cluster#getting-started) out.

A [port-forward script](./port-forward) script is included in this repository that can be used to test this director service against the Open-Match cluster. The script port-forwards the Kubernetes deployments of the Frontend-and backend services to their respective ports.

Once port-forwarded, tickets can be inserted by calling the frontend service:

```sh
curl http://localhost:51504/v1/frontendservice/tickets --data "{\"ticket\":{}}"
curl http://localhost:51504/v1/frontendservice/tickets --data "{\"ticket\":{}}"
```

In which the frontend service will reply with our ticket ids:

```sh
$ curl http://localhost:51504/v1/frontendservice/tickets --data "{\"ticket\":{}}"
{"id":"c9kjlqmd93fsf0895vig", "create_time":"2022-04-27T12:40:37.140383967Z"}
```

After inserting those two tickets (per the rules of the sample 1v1 match function), a session will be started which details can be looked up by watching the assignment:

```sh
$ curl -X GET http://localhost:51504/v1/frontendservice/tickets/c9kjlqmd93fsf0895vig/assignments
```

As the director has received a match, it has started a session and assigned our two tickets to said session:

```json
{"result":{"assignment":{"connection":"{\"host\":\"84.16.234.88\",\"ports\":[{\"type\":\"tcp\",\"container\":80,\"host\":49161},{\"type\":\"tcp\",\"container\":80,\"host\":49161}]}"}}}
```

The tickets can now be safely deleted.

## Debugging

The `.vscode` directory contains a launch task called `debug` that allows the service to be started locally against the Open-Match cluster above.

## Environment Variables

| Variable                 | Example Value                                   | Description                                                          |
|--------------------------|-------------------------------------------------|----------------------------------------------------------------------|
| GAMEYE_API_KEY           | this-is-my-test-token                           | The secret API key to gain access to Gameye as the server allocator. |

## Program Arguments

| Variable                 | Example Value                                   | Description                                                | Optional |
|--------------------------|-------------------------------------------------|------------------------------------------------------------|----------|
| gameye-api               | https://api.gameye.io                           | The url to Gameye's session API                            | No       |
| open-match-backend       | http://localhost:51505                          | The url to Open-Match's Backend Service.                   | No       |
| open-match-function      | om-function.open-match-demo.svc.cluster.local   | The in-cluster url to the Open-Match function to invoke.   | No       |
| open-match-function-port | 50502                                           | The port number of the Open-Match function.                | No       |
| region                   | europe                                          | The region to start a session in.                          | No       |
| image                    | nginx                                           | The image to start a session of.                           | No       |
| fetch-match-delay        | 1                                               | The delay between each fetch match poll in seconds.        | Yes      |

## License

This director service is available under the terms of the [WTFPL](http://www.wtfpl.net/) license. The full license is available in the `LICENSE` file.