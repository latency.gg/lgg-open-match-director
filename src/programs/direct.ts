import * as metaApi from "@latency.gg/lgg-meta-api";
import { createHttpAgentSendReceive } from "@oas3/http-send-receive";
import { isAbortError } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import * as https from "https";
import { second } from "msecs";
import * as prom from "prom-client";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";
import * as worker from "../worker/index.js";

program.
    command("direct").
    requiredOption("--gameye-api <url>", "URL for Gameye's Public API", v => new URL(v)).
    requiredOption("--open-match-backend <url>", "Backend url for Open-Match Backend Service", v => new URL(v)).
    requiredOption("--open-match-function <string>", "In-cluster url for the Open-Match Function").
    requiredOption("--open-match-function-port <number>", "The port number of the Open-Match Function").
    requiredOption("--region <string>", "The region to start sessions in").
    requiredOption("--image <string>", "The image to start a session of").
    option("--meta-port <number>", "Listen to meta-port", Number, 9090).
    option(
        "--meta-endpoint <url>",
        "endpoint of the meta-service",
        v => new URL(v),
        new URL("http://localhost:9090"),
    ).
    option("--shutdown-delay <msec>", "", Number, 35 * second).
    option("--connect-timeout <msec>", "", Number, 10 * second).
    option("--idle-timeout <msec>", "", Number, 40 * second).
    option("--fetch-match-delay <msec>", "", Number, 1 * second).
    action(programAction);

const GAMEYE_API_KEY = process.env["GAMEYE_API_KEY"];

interface ProgramConfig {
    gameyeApi: URL;
    openMatchBackend: URL;
    openMatchFunction: string;
    openMatchFunctionPort: number;
    region: string;
    image: string;
    metaPort: number;
    metaEndpoint: URL;
    shutdownDelay: number;
    connectTimeout: number;
    idleTimeout: number;
    fetchMatchDelay: number;
}
async function programAction(config: ProgramConfig) {
    const {
        gameyeApi,
        openMatchBackend,
        openMatchFunction,
        openMatchFunctionPort,
        region,
        image,
        metaPort,
        metaEndpoint,
        shutdownDelay,
        connectTimeout,
        idleTimeout,
        fetchMatchDelay,
    } = config;

    if (!GAMEYE_API_KEY || GAMEYE_API_KEY.length === 0) {
        throw new Error("missing $GAMEYE_API_KEY");
    }

    console.log("Starting...");
    console.log(`open-match-backend: ${openMatchBackend.host}`);
    console.log(`open-match-function: ${openMatchFunction}`);
    console.log(`open-match-function-port: ${openMatchFunctionPort}`);

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    const promRegistry = new prom.Registry();
    prom.collectDefaultMetrics({ register: promRegistry });

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const defaultAgent = new http.Agent({
        keepAlive: false,
    });

    const secureAgent = new https.Agent({
        keepAlive: true,
    });

    const metaConfig: metaApi.Config = {
        endpoint: metaEndpoint,

        livenessController,
        readinessController,

        promRegistry,
    };

    const metaContext = metaApi.createContext(metaConfig);

    const applicationConfig: application.Config = {
        gameyeApiKey: GAMEYE_API_KEY!,

        gameyeApiUrl: gameyeApi,
        openMatchBackendUrl: openMatchBackend,

        abortController: livenessController,
        promRegistry,

        httpSendReceive: createHttpAgentSendReceive({
            agent: defaultAgent,

            connectTimeout,
            idleTimeout,
        }),

        secureHttpSendReceive: createHttpAgentSendReceive({
            agent: secureAgent,

            connectTimeout,
            idleTimeout,
        }),
    };

    const workerConfig: worker.Config = {
        openMatchBackendUrl: openMatchBackend,

        openMatchFunctionUrl: openMatchFunction,
        openMatchFunctionPort,

        fetchMatchDelay,
        region,
        image,

        httpSendReceive: applicationConfig.httpSendReceive,
    };

    const applicationContext = application.createContext(applicationConfig);
    try {

        /*
        We use the waitgroup to wait for requests to finish
        */
        const waitGroup = new WaitGroup();

        const metaServer = metaApi.createServer(
            metaContext,
            onWarn,
        );
        metaServer.registerMiddleware(
            (route, request, next) => waitGroup.with(() => next(request)),
        );

        const metaHttpServer = http.createServer(metaServer.asRequestListener({
            onError: onWarn,
        }));

        await new Promise<void>(resolve => metaHttpServer.listen(
            metaPort,
            () => resolve(),
        ));

        try {
            console.log("Started");

            try {
                await worker.runLoop(
                    applicationContext,
                    workerConfig,
                    readinessController.signal,
                    onWarn,
                );
            }
            catch (error) {
                if (!isAbortError(error)) throw error;
            }

            console.log("Stopping...");

            /*
            wait a bit, usually for the load balancer to recognize this service as
            not ready and move traffic to another service.
            */
            await delay(shutdownDelay);

            /*
            wait for all requests to finish
            */
            await waitGroup.wait();

            /*
            abort the liveness controller to terminate all streaming responses!
            */
            livenessController.abort();

        }
        finally {
            await new Promise<void>((resolve, reject) => metaHttpServer.close(
                error => error ?
                    reject(error) :
                    resolve(),
            ));
        }
    }
    finally {
        await applicationContext.destroy();
    }

    console.log("Stopped");
}
