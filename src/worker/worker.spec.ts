import * as openMatchBackend from "@latency.gg/lgg-open-match-backend-oas";
import * as openMatchFrontend from "@latency.gg/lgg-open-match-frontend-oas";
import { createHttpAgentSendReceive } from "@oas3/http-send-receive";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { v4 as uuidv4 } from "uuid";
import { withContext } from "../testing/index.js";
import { Config, createOpenMatchBackendClient, runOnce } from "./worker.js";

test.skip("purge all tickets", async t => {
    const frontendClient = new openMatchFrontend.Client({
        baseUrl: new URL("http://localhost:51504"),
    });

    const backendClient = new openMatchBackend.Client({
        baseUrl: new URL("http://localhost:51505"),
    });

    const matchProfileSchema: openMatchBackend.OpenmatchMatchProfileSchema = {
        name: "1v1",
        pools: [
            {
                name: "Everyone",
            },
        ],
    };

    const matchFunctionSchema: openMatchBackend.OpenmatchFunctionConfigSchema = {
        host: "om-function.open-match-demo.svc.cluster.local",
        port: 50502,
        type: "GRPC",
    };

    const request = {
        config: matchFunctionSchema,
        profile: matchProfileSchema,
    } as openMatchBackend.OpenmatchFetchMatchesRequestSchema;

    const response = await backendClient.backendServiceFetchMatches({
        parameters: {},
        entity() {
            return request;
        },
    });

    if (response.status !== 200) {
        throw new Error();
    }

    const stream = response.entities();
    for await (const { result, error } of stream) {
        const tickets = result!.match!.tickets!;

        for (const ticket of tickets) {
            const res = await frontendClient.frontendServiceDeleteTicket({
                parameters: {
                    ticketId: ticket.id!,
                },
            });

            if (res.status !== 200) {
                throw new Error();
            }
        }
    }
});

test("worker should assign tickets to an ignited session", async t => withContext(async ctx => {
    const ticketCount = 10;

    let assignmentCounter = 0;

    {
        ctx.servers.gameyeApi.registerApiTokenAuthorization(async incomingRequest => {
            return "123";
        });

        ctx.servers.gameyeApi.registerSessionRunOperation(async incomingRequest => {
            return {
                status: 201,
                parameters: {},
                contentType: "application/json",
                async entity() {
                    return {
                        host: "127.0.0.1",
                        ports: [
                            {
                                type: "tcp",
                                container: 8080,
                                host: 36844,
                            },
                        ],
                    };
                },
            };
        });

        // eslint-disable-next-line max-len
        ctx.servers.openMatchBackend.registerBackendServiceFetchMatchesOperation(async incomingRequest => {
            return {
                status: 200,
                parameters: {},
                contentType: "application/json",
                async *entities(signal: AbortSignal) {
                    const tickets: openMatchBackend.OpenmatchTicketSchema[] = [];
                    for (let i = 0; i < ticketCount; i++) {
                        tickets.push({
                            id: uuidv4(),
                        });
                    }

                    yield {
                        result: {
                            match: {
                                tickets,
                            },
                        },
                    };
                },
            };
        });

        // eslint-disable-next-line max-len
        ctx.servers.openMatchBackend.registerBackendServiceAssignTicketsOperation(async incomingRequest => {
            return {
                status: 200,
                parameters: {},
                contentType: "application/json",
                async entity() {
                    const groups = await incomingRequest.entity();
                    assignmentCounter += groups.assignments?.length ?? 0;

                    return {};
                },
            };
        });
    }

    {
        const abortController = new AbortController();
        const config: Config = {
            openMatchBackendUrl: ctx.endpoints.openMatchBackend,

            openMatchFunctionUrl: "om-function.open-match-demo.svc.cluster.local",
            openMatchFunctionPort: 50502,

            fetchMatchDelay: 1 * second,

            region: "europe",
            image: "nginx",

            httpSendReceive: createHttpAgentSendReceive({}),
        };

        const client = createOpenMatchBackendClient(
            config.openMatchBackendUrl,
            config.httpSendReceive!,
        );

        await runOnce(
            client,
            ctx.appContext,
            config,
            abortController.signal,
            () => t.fail(),
        );

        t.equal(assignmentCounter, 1);
    }
}));
