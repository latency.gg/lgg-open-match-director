import { HttpSendReceive } from "@oas3/http-send-receive";
import * as prom from "prom-client";

export interface Config {
    gameyeApiKey: string;

    gameyeApiUrl: URL;
    openMatchBackendUrl: URL;

    abortController: AbortController,
    promRegistry: prom.Registry;

    httpSendReceive?: HttpSendReceive;
    secureHttpSendReceive?: HttpSendReceive;
}
